import './main.css';

import React from 'react';

import {
    useQuery,
    gql
  } from "@apollo/client";


  const ClientListQuery = gql`
  query ExampleQuery {
    clients {
      name, totalMonthlyCost, address, rating,products {phone, type, cost}, boxes {typeid, param1, param2, param3, param4, param5}
    }
  }
`;


class TitleComponent extends React.Component {
    constructor(props) {
      super(props);
      this.state = {date: new Date()};
    }
  
    componentDidMount() {  }
    componentWillUnmount() {  }
    render() {
      return (
        <div class="title">Profile & Products</div>
      );
    }
  }

  function ScoreComponent(props) {
    const listGoldStar = [...Array(props.rating).keys()].map(i => <div class="star gold"></div>);
    const listStar = [...Array(5 - props.rating).keys()].map(i => <div class="star"></div>)
      return (
    <div class="score">
      <div class="status gold">GOLD</div>
      {listGoldStar}
      {listStar}
      <div class="result">satisfied</div>
      </div>
      );
  }

  function InfoComponent(props)  {
    return  (
      <div class="infromation">
      <div><p class="left">Total monthly cost</p><p class="right">{props.data.totalMonthlyCost}</p></div>
      <div><p class="left">National ID number</p><p class="right underline">Show National ID number</p></div>
      <div><p class="left">PESEL</p><p class="right underline">Show PESEL</p></div>
      <div><p class="left">Password</p><p class="right underline">Show Password</p></div>
      <div><p class="left">Address</p><p class="right">{props.data.address}</p></div>
      </div>
    );

  }

  function Products(props)  {

    const listItems = props.products.map((product) => 
    
    <ProductComponent2 phone={product.phone} type={product.type} cost={product.cost}/>)

    return  (
      <div class="products">
      {listItems}
      </div>
    );

  }

  function Boxes(props)  {

    const listItems = props.boxes.map((box) => 
      box.typeid == 2 ? <BoxComponent2 name={box.param1} role={box.param2} age={box.param3} interests={box.param4}/>
    : box.typeid == 3 ? <BoxComponent3 name={box.param1} type={box.param2} owner={box.param3} data={box.param4} runtime={box.param5}/>
    : box.typeid == 4 ? <BoxComponent4 name={box.param1} type={box.param2} owner={box.param3} data={box.param4} runtime={box.param5}/>
    : box.typeid == 5 ? <BoxComponent5 name={box.param1} type={box.param2} text={box.param3}/> 
    : <BoxComponent2 name="Jan Wiatrek"   role="Father | Mobile M" age="31" interests="Football, Books3"/> )

    return  (
      <div class="other">
        {listItems}
      </div>
    );

  }

  class ProductComponent2 extends React.Component {
    constructor(props) {
      super(props);
      this.state = props
    }
  
    componentDidMount() {  }
    componentWillUnmount() {  }
    render() {
      return (
        <div class="product-2">
          <div class="phone">{this.state.phone}</div>
          <div class="green"></div>
          <div class="type">{this.state.type}</div>
          <div class="cost">{this.state.cost}</div>
      </div>
      );
    }
  }

  class BoxComponent2 extends React.Component {
    constructor(props) {
      super(props);
      this.state = props
    }
  
    componentDidMount() {  }
    componentWillUnmount() {  }
    render() {
      return (
        <div class="box">
          <div class="info">
              <div class="face"></div>
              <div class="name">
                  <span>{this.state.name}</span>
                  <span>{this.state.role}</span>
              </div>
              <div class="question"></div>
          </div>
          <div class="information2">
              <div><p class="left">Age</p><p class="right">{this.state.age}</p></div>
              <div><p class="left">Interests</p><p class="right">{this.state.interests}</p></div>
          </div>
      </div>
      );
    }
  }

  class BoxComponent3 extends React.Component {
    constructor(props) {
      super(props);
      this.state = props
    }
  
    componentDidMount() {  }
    componentWillUnmount() {  }
    render() {
      return (
        <div class="box">
          <div class="info">
              <div class="mobile"></div>
              <div class="name">
                  <span>{this.state.name}</span>
                  <span>{this.state.type}</span>
              </div>
              <div class="question"></div>
          </div>
          <div class="information2">
              <div><p class="left">Owner</p><p class="right">{this.state.owner}</p></div>
              <div><p class="left">Data</p><p class="right">{this.state.data}</p></div>
              <div><p class="left">Runtime</p><p class="right">{this.state.runtime}</p></div>
          </div>
          <div class="expiried"><span>Expires soon</span></div>
      </div>
      );
    }
  }

  class BoxComponent4 extends React.Component {
    constructor(props) {
      super(props);
      this.state = props
    }
  
    componentDidMount() {  }
    componentWillUnmount() {  }
    render() {
      return (
        <div class="box">
          <div class="info">
              <div class="mobile"></div>
              <div class="name">
                  <span>{this.state.name}</span>
                  <span>{this.state.type}</span>
              </div>
              <div class="question"></div>
          </div>
          <div class="information2">
              <div><p class="left"></p><p class="right">{this.state.owner}</p></div>
              <div><p class="left"></p><p class="right">{this.state.data}</p></div>
              <div><p class="left"></p><p class="right">{this.state.runtime}</p></div>
          </div>
          <div class="not_poland"><span>NOT AT T-MOBILE POLAND</span></div>
      </div>
      );
    }
  }

  class BoxComponent5 extends React.Component {
    constructor(props) {
      super(props);
      this.state = props
    }
  
    componentDidMount() {  }
    componentWillUnmount() {  }
    render() {
      return (
        <div class="box">
          <div class="info">
              <div class="home"></div>
              <div class="name-hide">
                  <span>{this.state.name}</span>
                  <span>{this.state.type}</span>
              </div>
              <div class="question"></div>
          </div>
          <div class="info short">
              <div class="internet"></div>
              <div class="text">{this.state.text}</div>
          </div>
          <div class="not_poland"><span>NOT AT T-MOBILE POLAND</span></div>
      </div>
      );
    }
  }

  class PredictionBox extends React.Component {
    constructor(props) {
      super(props);
      this.state = props
    }
  
    componentDidMount() {  }
    componentWillUnmount() {  }
    render() {
      return (
        <div class="box small">
          <div class="prediction"><span>Prediction</span></div>
          <div class="info">
              <div class="face hide"></div>
              <div class="name-hide">
                  <span>{this.state.name}</span>
                  <span>{this.state.role}</span>
              </div>
              <div class="question"></div>
          </div>
          <div class="information2">
              <div><p class="left">Age</p><p class="right">{this.state.age}</p></div>
              <div><p class="left">Interests</p><p class="right">{this.state.interests}</p></div>
          </div>
      </div>
      );
    }
  }

function CustomerProfile(props)
{
  const { loading, error, data } = useQuery(ClientListQuery);
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  console.log(data);
  var index = Math.random()<0.5?0:1


  return <div class="center">
  <TitleComponent/>
  <ScoreComponent rating={data.clients[index].rating}/>
  <InfoComponent data={data.clients[index]}/>
  <Products products={data.clients[index].products}/>
  <div class="add_products">
      add product
  </div>
  <Boxes boxes={data.clients[index].boxes}/>
</div>
} 


function App() {
  return (
    <CustomerProfile/>    
  );
}

export default App;
